import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObygnComponent } from './obygn.component';

describe('ObygnComponent', () => {
  let component: ObygnComponent;
  let fixture: ComponentFixture<ObygnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObygnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObygnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
